from django.contrib import admin

# Register your models here.
from django.db.models import get_models, get_app

# Register your models here.
for model in get_models(get_app('gsp15')):
    admin.site.register(model)
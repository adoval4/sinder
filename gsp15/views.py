from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, HttpResponseRedirect

from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout

from django.db import IntegrityError
from django.core.exceptions import ObjectDoesNotExist

from django.contrib.auth.decorators import login_required
from django.core.mail import EmailMultiAlternatives

from models import *

import random, string

# Create your views here.
@csrf_exempt
def index(request):
	if request.user.is_authenticated():
		if request.method == 'GET':
			# user = request.user
			# evaluations = list(Picked.objects.filter(user=user.id).filter(elected__isnull=True))
			# random.shuffle(evaluations)
			# if len(evaluations) > 0:
			# 	for evaluation in evaluations:
			# 		if evaluation.elected is None:
			# 			participant_to_eval = Participant.objects.get(user__id=evaluation.picked_user.id)
			# 			context_data = {
			# 				'base_url': '//'+request.get_host(),			
			# 				'participant': participant_to_eval,
			# 				'participant_photo': participant_to_eval.image.url[2:],
			# 			}
			# 			return render(request, 'index.html', context_data)

			context_data = {
				'base_url': '//'+request.get_host(),			
			}
			return render(request, 'end.html', context_data)
			# return HttpResponse('FAIL')

		elif request.method == 'POST':
			user = request.user
			
			participant_id = int(request.POST.get("id"))
			participant_eval = int(request.POST.get("val"))
			participant_evaluation_bool = True
			if participant_eval == 0:
				participant_evaluation_bool = False
			else:
				participant_evaluation_bool = True

			print '%d\t%d\t%s'%(user.id, participant_id, str(participant_eval))

			participant_evaluations = Picked.objects.filter(user__id=user.id)
			eval_participant = participant_evaluations.get(picked_user__id=participant_id)

			if eval_participant is not None:
				eval_participant.elected = participant_eval 
				eval_participant.save()
				return HttpResponse('OK')
			else:
				print 'Already evaluated'
				return HttpResponse('FAIL')
	return HttpResponseRedirect('/login/')

def create_users(request):
	# participants = Participant.objects.all()
	participants = Participant.objects.all()
	participants_1 = Participant.objects.filter(user__username='adolfo.valdivieso.quiroz')

	# for participant in participants_1:
	# 	for participant1 in participants:
	# 		if participant.user.id != participant1.user.id:
	# 			new_pic = Picked.objects.filter(user=participant.user, picked_user= participant1.user)
	# 			if len(new_pic) == 0:
	# 				new_pic = Picked.objects.create(user=participant.user, picked_user= participant1.user, elected=None)
	# 				new_pic.save()

	# 	new_pass = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(5))
	# 	try:
	# 		new_cred = Credential.objects.get(user=participant.user)
	# 		new_cred.password = new_pass
	# 	except ObjectDoesNotExist: 
	# 		new_cred = Credential.objects.create(user=participant.user, password=new_pass)

	# 	user = participant.user

	# 	user.set_password(new_pass)

	# 	new_cred.save()
	# 	user.save()

	evals = Picked.objects.all()

	for evaluation in evals:
		if evaluation.user.id == participants_1[0].user.id:
			evaluation.elected = True
			evaluation.save()

	return HttpResponse('OK')

def send_emails(request, user_id):
	# participants = Participant.objects.all()
	participant = Participant.objects.get(user__id=user_id)
	credential = Credential.objects.get(user=participant.user.id)

	email = participant.user.email

	subject, from_email, to = 'Welcome to SUinder', 'noreply@adolfovaldivieso.com', email
	text_content = 'This is an important message.'
	html_content = "<html>"+\
			"<body>"+\
			"<p>"+\
			"Hi! Welcome to SUinder: the group making tool for GSP15."+\
			"</p>"+\
			"<p>"+\
			"We will show you the profile (based on Habib's spreadsheet) of each participant and you have to choose YES if you want to work with him/her and NO if not."+\
			"</p>"+\
			"<p>"+\
			"<strong>We will send you an email with your matches at tuesday 14th at midnight.</strong>"+\
			"</p>"+\
			"<p>"+\
			"Here you have your credentials:"+\
			"</p>"+\
			"<p>"+\
			"username: "+participant.user.username+"<br>"+\
			"password: "+credential.password+\
			"</p>"+\
			"<p>"+\
			"Go to: <a href='http://www.adolfovaldivieso.com/login/' target='_blank'>adolfovaldivieso.com/login</a>"+\
			"</p>"+\
			"<p>"+\
			"Pdt: Remember that friends are forever. Let's make this experience the best."+\
			"</p>"+\
			"<p>"+\
			"Hugs,"+\
			"Adolfo V and Ana Karen Ramirez."+\
			"</p>"+\
		"</body>"+\
		"</html>"

	return HttpResponse(html_content)

def matching(request, user_id):

	# all_participants = Participant.objects.all()
	participant = Participant.objects.get(user__id=user_id)
	evaluations = Picked.objects.filter(user=participant.user)

	matches = list()
	matches_txt = ''
	all_none = True
	for evaluation in evaluations:
		# if evaluation.elected == True:
		reverse_evaluation = Picked.objects.get(user=evaluation.picked_user, picked_user=participant.user)
		if reverse_evaluation.elected == True:
			match_name = evaluation.picked_user.first_name+' '+evaluation.picked_user.last_name
			match_name = match_name.upper()
			matches.append(match_name)
			matches_txt += '- '+match_name+'<br>'
		# if evaluation.elected is not None:
		all_none = False

	if all_none is False:
		html_content = "<html>"+\
				"<body>"+\
				"<p>"+\
				"Hi "+participant.user.first_name.upper()+' '+participant.user.last_name.upper()+"!"+\
				"</p>"+\
				"<p>"+\
				"Here you have your matches:"+\
				"</p>"+\
				"<p>"+\
				matches_txt+\
				"</p>"+\
				"<p>"+\
				"Let's talk with them! We hope you find this useful."+\
				"</p>"+\
				"<p>"+\
				"Pdt: Remember that friends are forever. Let's make this experience the best."+\
				"</p>"+\
				"<p>"+\
				"Hugs,<br>"+\
				"Adolfo V and Ana Karen Ramirez."+\
				"</p>"+\
			"</body>"+\
			"</html>"
	else:
		html_content = participant.user.username
	return HttpResponse(html_content)

def connections(request):
	participants = Participant.objects.all()
	evaluations = Picked.objects.all()
	
	ids = []
	connections = []

	for participant in participants:
		ids.append(participant.user.username)

	for evaluation in evaluations:
		if evaluation.elected is not None:
			if evaluation.elected is True:
				con = (str(evaluation.user.username), str(evaluation.picked_user.username), 1)
			else:
				con = (str(evaluation.user.username), str(evaluation.picked_user.username), 0)
			connections.append(con)

	html = str(ids) + '<br><br>' + str(connections)
	return HttpResponse(html)

@csrf_exempt
def user_login(request):
	if request.method == 'GET':
		context_data = {
			'base_url': '//'+request.get_host(),			
		}
		return render(request, 'login.html', context_data)
	elif request.method == 'POST':
		try:
			username = request.POST['username']
			password = request.POST['password']
			print request.POST
			user = authenticate(username=username, password=password)
			print user
			if user is not None:
				if user.is_active:
					login(request, user)
					return HttpResponse('OK')
			return HttpResponse('FAIL')
		except Exception as e:
			print e
			return HttpResponse('FAIL')

def user_logout(request):
	logout(request)
	return HttpResponseRedirect('../login/')
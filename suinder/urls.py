"""suinder URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
	https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
	1. Add an import:  from my_app import views
	2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
	1. Add an import:  from other_app.views import Home
	2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
	1. Add an import:  from blog import urls as blog_urls
	2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.contrib import admin
from gsp15 import views

urlpatterns = [
	url(r'^12345/aaaa/matches/(?P<user_id>\d+)', views.matching),
	url(r'^12345/connections', views.connections),
	url(r'^12345/create/', views.create_users),
	url(r'^admin/', include(admin.site.urls)),
	url(r'^login/$', views.user_login),
	url(r'^logout/$', views.user_logout),
	# url(r'^send/(?P<user_id>\d+)', views.send_emails),
	url(r'^$', views.index),
]

from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Participant(models.Model):
	user = models.OneToOneField(User)
	country = models.CharField(blank=True, default='', max_length=30)
	image = models.ImageField()

	logged = models.BooleanField(default=False)

	challenge_interest = models.TextField(blank=True, default='', max_length=1000)
	tech_interest = models.TextField(blank=True, default='', max_length=1000)
	team_wishes = models.TextField(blank=True, default='', max_length=1000)
	project_wishes = models.TextField(blank=True, default='', max_length=1000)
	experience = models.TextField(blank=True, default='', max_length=1000)
	after_gsp_plans = models.TextField(blank=True, default='', max_length=1000)

	def __unicode__(self):
		names = self.user.first_name+' '+self.user.last_name
		return u'%d %s - %s'%(self.user.id, names, self.country)


class Picked(models.Model):
	user = models.ForeignKey(User, related_name="user")
	picked_user = models.ForeignKey(User, related_name="picked")
	elected = models.NullBooleanField()

	def __unicode__(self):
		if self.elected is None:
			return u'%s to %s'%(self.user.username, self.picked_user.username)
		elif self.elected is True:
			return u'%s to %s says YES'%(self.user.username, self.picked_user.username)
		elif self.elected is False:
			return u'%s to %s says NO'%(self.user.username, self.picked_user.username)

	class Meta:
		ordering = ['user']

class Credential(models.Model):
	user = models.OneToOneField(User)
	password = models.CharField(max_length=30)

	def __unicode__(self):
		return u'%s - %s'%(self.password, self.user.username, )
